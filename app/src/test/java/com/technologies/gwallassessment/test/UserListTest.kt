package com.technologies.gwallassessment.test

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.technologies.gwallassessment.core.data.db.dao.UserDataDao
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.core.extension.getOrAwaitValue
import com.technologies.gwallassessment.core.network.repo.UsersRepository
import com.technologies.gwallassessment.feature.dashboard.userlist.UsersViewModel
import com.technologies.gwallassessment.utils.getUsers
import com.technologies.gwallassessment.utils.mockUsers
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import junit.framework.Assert.assertEquals
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

@RunWith(MockitoJUnitRunner::class)
class UserListTest {

    @MockK
    lateinit var usersViewModel: UsersViewModel

    @MockK(relaxed = true)
    lateinit var usersRepository: UsersRepository

    @MockK(relaxed = true)
    lateinit var usersObserver: Observer<List<UserData>>

    @MockK(relaxed = true)
    lateinit var emptyObserver: Observer<Boolean>

    @get:Rule
    val rule = InstantTaskExecutorRule()


    private val immediateScheduler: Scheduler = object : Scheduler() {

        override fun createWorker() = ExecutorScheduler.ExecutorWorker(Executor { it.run() }, true)

        // This prevents errors when scheduling a delay
        override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
            return super.scheduleDirect(run, 0, unit)
        }

    }


    @Before
    fun setup() {

        MockKAnnotations.init(this)

        RxJavaPlugins.setIoSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { immediateScheduler }

        setupMockResponses()

        usersViewModel = UsersViewModel(usersRepository)
        usersViewModel.apply {
            users.observeForever(usersObserver)
            empty.observeForever(emptyObserver)
        }
    }

    private fun setupMockResponses() {
        every { usersRepository.getUsers(0, true, 5) } returns getUsers(0)
        every { usersRepository.getUsers(5, true, 5) } returns getUsers(5)
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }

    @Test
    fun `Fetch user list success`() {
        usersViewModel.run {
            setHasInternetConnection(true)
            getUsers()

            //Check if same users with mockUsers was fetched
            assertEquals(mockUsers.subList(0, 5), users.getOrAwaitValue())

            verify {
                //Verify getUsers call
                usersRepository.getUsers(0, true, 5)
                //Verify insert user was called
                usersRepository.insertUsers(mockUsers.subList(0, 5))
            }

            confirmVerified(usersRepository)
        }
    }

    @Test
    fun `Fetch user list empty`() {
        usersViewModel.run {

            every { usersRepository.getUsers(0, true, 5) } returns getUsers(-1)

            setHasInternetConnection(true)
            getUsers()

            assertEquals(users.getOrAwaitValue().isEmpty(), true)

            val emptyCapture = slot<Boolean>()

            //verify that empty set value was called 2 times (during initialization and actual set value)
            verify(exactly = 2) {
                emptyObserver.onChanged(
                    capture(emptyCapture)
                )
            }

            assertEquals(emptyCapture.captured, true)

            verify {
                //Verify getUsers is only called and not insertUsers
                usersRepository.getUsers(0, true, 5)
            }

            confirmVerified(usersRepository)
        }
    }
}
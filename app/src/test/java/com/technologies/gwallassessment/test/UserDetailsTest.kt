package com.technologies.gwallassessment.test

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.technologies.gwallassessment.core.data.db.dao.LikeDataDao
import com.technologies.gwallassessment.core.data.db.dao.UserDataDao
import com.technologies.gwallassessment.core.data.db.entity.LikeData
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.core.network.repo.UsersRepository
import com.technologies.gwallassessment.feature.dashboard.userdetails.UserDetailsViewModel
import com.technologies.gwallassessment.utils.getUserData
import com.technologies.gwallassessment.utils.mockUsers
import com.technologies.gwallassessment.utils.userDetailsDefunkt
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

@RunWith(MockitoJUnitRunner::class)
class UserDetailsTest {

    @MockK
    lateinit var userDetailsViewModel: UserDetailsViewModel

    @MockK(relaxed = true)
    lateinit var usersRepository: UsersRepository

    @MockK(relaxed = true)
    lateinit var userDetailsObserver: Observer<UserData>

    @MockK(relaxed = true)
    lateinit var likedObserver: Observer<Boolean>

    @get:Rule
    val rule = InstantTaskExecutorRule()


    private val immediateScheduler: Scheduler = object : Scheduler() {

        override fun createWorker() = ExecutorScheduler.ExecutorWorker(Executor { it.run() }, true)

        // This prevents errors when scheduling a delay
        override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
            return super.scheduleDirect(run, 0, unit)
        }

    }


    @Before
    fun setup() {

        MockKAnnotations.init(this)

        RxJavaPlugins.setIoSchedulerHandler { immediateScheduler }
        RxJavaPlugins.setComputationSchedulerHandler { immediateScheduler }
        RxJavaPlugins.setNewThreadSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { immediateScheduler }

        setupMockResponses()

        userDetailsViewModel = UserDetailsViewModel(usersRepository)
        userDetailsViewModel.apply {
            userDetails.observeForever(userDetailsObserver)
            liked.observeForever(likedObserver)
        }
    }

    private fun setupMockResponses() {
        every { usersRepository.getUserDetails("defunkt", true) } returns getUserData(1)
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
        clearAllMocks()
    }


    @Test
    fun `On open details screen and set UserData from extra or arguments`() {
        userDetailsViewModel.run {
            setHasInternetConnection(true)
            setUserDetails(mockUsers[1])

            val userDetailsCapture = slot<UserData>()

            verify {
                userDetailsObserver.onChanged(
                    capture(userDetailsCapture)
                )
            }

            //Check if userDetails LiveData got defunkt initial user details
            assertEquals("defunkt", userDetailsCapture.captured.login)

            confirmVerified(userDetailsObserver)
        }
    }

    @Test
    fun `On set UserData from arguments and fetch complete user details`() {
        userDetailsViewModel.run {
            setHasInternetConnection(true)
            setUserDetails(mockUsers[1])
            getUserDetails(mockUsers[1].login)

            val userDetailsCapture = slot<UserData>()

            verify(exactly = 2) {
                userDetailsObserver.onChanged(
                    capture(userDetailsCapture)
                )
            }

            //Check if userDetails LiveData got defunkt initial user details
            assertEquals("defunkt", userDetailsCapture.captured.login)
            //Check if userDetails LiveData got full data from getUserDetails call by
            //checking data not available on initial userData value `mockUsers[1]`
            assertEquals("Chris Wanstrath", userDetailsCapture.captured.name)
            assertEquals("http://chriswanstrath.com/", userDetailsCapture.captured.blog)

            verify {
                //Verify that getUserDetails is called in UserRepository
                usersRepository.getUserDetails(mockUsers[1].login, true)
                //Verify that insertUserData is called in UserRepository
                usersRepository.insertUserData(userDetailsDefunkt)
                //Verify that getLikeByUserId is called in UserRepository
                usersRepository.getLikeByUserId(mockUsers[1].id)
            }

            confirmVerified(userDetailsObserver)
            confirmVerified(usersRepository)
        }
    }

    @Test
    fun `On set UserData from arguments and fetch complete user details and hit like`() {
        userDetailsViewModel.run {
            setHasInternetConnection(true)
            setUserDetails(mockUsers[1])
            getUserDetails(mockUsers[1].login)

            val userDetailsCapture = slot<UserData>()
            val likeCapture = slot<Boolean>()

            filteredLike(true)

            verify(exactly = 2) {
                userDetailsObserver.onChanged(
                    capture(userDetailsCapture)
                )
            }

            verify {
                likedObserver.onChanged(
                    capture(likeCapture)
                )
            }

            //Check if userDetails LiveData got defunkt initial user details
            assertEquals("defunkt", userDetailsCapture.captured.login)
            //Check if userDetails LiveData got full data from getUserDetails call by
            //checking data not available on initial userData value `mockUsers[1]`
            assertEquals("Chris Wanstrath", userDetailsCapture.captured.name)
            assertEquals("http://chriswanstrath.com/", userDetailsCapture.captured.blog)
            //Check if like value is updated
            assertEquals(true, likeCapture.captured)

            verify {
                //Verify that getUserDetails is called in UserRepository
                usersRepository.getUserDetails(mockUsers[1].login, true)
                //Verify that insertUserData is called in UserRepository
                usersRepository.insertUserData(userDetailsDefunkt)
                //Verify that getLikeByUserId is called in UserRepository
                usersRepository.getLikeByUserId(mockUsers[1].id)
                usersRepository.setUserLike(LikeData(mockUsers[1].id), true)
            }

            confirmVerified(userDetailsObserver)
            confirmVerified(likedObserver)
            confirmVerified(usersRepository)
        }
    }
}
package com.technologies.gwallassessment.core.di.module

import androidx.annotation.NonNull
import com.technologies.gwallassessment.BuildConfig
import com.technologies.gwallassessment.core.di.scope.AppScope
import com.technologies.gwallassessment.core.network.AppApi

import java.util.concurrent.TimeUnit

import dagger.Module
import dagger.Provides
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

const val BACKEND_BASE_URL = "https://api.github.com/"

@Module
class NetworkModule {

    /**
     * Logging interceptor for okHttp to provide api logs if build is DEBUG
     */
    internal val loggingInterceptor: HttpLoggingInterceptor
        @Provides
        get() = HttpLoggingInterceptor().setLevel(
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        )

    @Provides
    fun getBackendApiEndpoint(): String {
        return BACKEND_BASE_URL
    }

    /**
     * Retrofit builder
     */
    @Provides
    @AppScope
    internal fun provideApiRetrofit(
        @NonNull baseUrl: String, client: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    /**
     * OkHttp builder
     */
    @Provides
    @AppScope
    internal fun getHttpClient(
        interceptor: HttpLoggingInterceptor,
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder
            .addInterceptor(httpApiInterceptor())
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)

        builder.addInterceptor(interceptor)

        return builder.build()
    }

    /**
     * Api interceptor
     */
    private fun httpApiInterceptor(): Interceptor {
        return Interceptor { chain ->
            var request = chain.request()
            val requestBuilder = request.newBuilder()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
            request = requestBuilder.build()
            chain.proceed(request)
        }
    }

    @Provides
    @AppScope
    fun provideApiService(retrofit: Retrofit): AppApi {
        return retrofit.create(AppApi::class.java)
    }

}


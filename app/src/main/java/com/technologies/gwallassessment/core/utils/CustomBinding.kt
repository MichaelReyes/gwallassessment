package com.technologies.gwallassessment.core.utils

import androidx.databinding.BindingAdapter

@BindingAdapter("imageUrl")
fun loadImage(view: GlideImageView, url: String?) {
    if (!url.isNullOrEmpty()) {
        view.setImageUrl(url)
    }
}
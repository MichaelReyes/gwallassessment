package com.technologies.gwallassessment.core.data.diffutil

import androidx.recyclerview.widget.DiffUtil
import com.technologies.gwallassessment.core.data.db.entity.UserData

class UsersDiffUtilItemCallback : DiffUtil.ItemCallback<UserData>() {
    override fun areItemsTheSame(oldItem: UserData, newItem: UserData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: UserData, newItem: UserData): Boolean {
        return oldItem.login == newItem.login
    }
}

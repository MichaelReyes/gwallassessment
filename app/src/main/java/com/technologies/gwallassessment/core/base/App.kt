package com.technologies.gwallassessment.core.base

import android.net.NetworkInfo
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.jakewharton.rxrelay3.BehaviorRelay
import com.technologies.gwallassessment.core.di.components.DaggerApplicationComponent
import com.technologies.gwallassessment.core.utils.AppLogger
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers


class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.factory().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        initNetworkObserver()
        initLogger()
    }

    private val disposables = CompositeDisposable()

    /**
     * Network observer using BehaviorRelay where activity and fragments can subscribe
     * and get internet connection status
     */
    private fun initNetworkObserver() {

        ReactiveNetwork
            .observeNetworkConnectivity(applicationContext)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                internetConnectionStream.accept(it.available() && it.state() == NetworkInfo.State.CONNECTED)
            }.addTo(disposables)
    }

    val internetConnectionStream = BehaviorRelay.create<Boolean>()

    /**
     * Timber logging
     */
    private fun initLogger() {
        AppLogger.plantTree()
    }
}
package com.technologies.gwallassessment.core.network

import com.technologies.gwallassessment.core.data.db.entity.UserData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface AppApi {

    @GET("users")
    fun getUsers(@Query("since") start: Int): Single<List<UserData>>

    @GET("users/{user_login}")
    fun getUserDetails(@Path("user_login") login: String): Single<UserData>

}
package com.technologies.gwallassessment.core.di.components

import com.technologies.gwallassessment.core.base.App
import com.technologies.gwallassessment.core.di.module.*
import com.technologies.gwallassessment.core.di.scope.AppScope
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@AppScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        ActivityBuilderModule::class,
        FragmentBuilderModule::class,
        NetworkModule::class,
        DatabaseModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<App> {
    @Component.Factory
    interface Factory: AndroidInjector.Factory<App>
}
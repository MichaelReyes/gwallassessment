package com.technologies.gwallassessment.core.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ColorMatrixColorFilter
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.technologies.gwallassessment.R
import java.io.File

/**
 * ImageView wrapped with Glide to handle image loading
 */
class GlideImageView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
    AppCompatImageView(context, attrs, defStyle) {

    fun setImageUrl(imageUrl: String?) {
        if (imageUrl == null)
            return
        Glide.with(context)
            .load(imageUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions().placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder)))
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(this)
    }

    fun setImageUrlWithFilter(imageUrl: String?, filter: FloatArray) {
        if (imageUrl == null)
            return
        Glide.with(context)
            .load(imageUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions().placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder)))
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(object: CustomTarget<Drawable>(){
                override fun onLoadCleared(placeholder: Drawable?) {}

                override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                    resource.colorFilter = ColorMatrixColorFilter(filter)
                    setImageDrawable(resource)
                }
            })
    }

    fun setImageUrlRound(imageUrl: String?) {
        if (imageUrl == null)
            return
        Glide.with(context)
            .load(imageUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(
                RequestOptions()
                    .transform(CenterCrop(), RoundedCorners(64))
                    .placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder))
            )
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(this)
    }

    fun setImageUri(imageUri: Uri?) {
        if (imageUri == null)
            return
        Glide.with(context)
            .load(imageUri)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions().placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder)))
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(this)
    }

    fun setImageFile(file: File?) {
        if (file == null)
            return
        Glide.with(context)
            .load(file)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions().placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder)))
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(this)
    }

    fun setImageFromDrawable(drawable: Drawable) {
        Glide.with(context)
            .load(drawable)
            .apply(
                RequestOptions()
                    .transform(CenterCrop(), RoundedCorners(64))
                    .placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder))
            )
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(this)
    }

    fun setImageBitmap(bitmap: Bitmap, placeholder: Drawable) {
        Glide.with(context)
            .load(bitmap)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(this)
    }

}

package com.technologies.gwallassessment.core.data.datasource

import androidx.lifecycle.LiveData
import androidx.paging.ItemKeyedDataSource
import com.technologies.gwallassessment.core.data.db.dao.UserDataDao
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.core.extension.ioToMainSubscribe
import com.technologies.gwallassessment.core.network.repo.UsersRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

/**
 * For handling user fetching with pagination using ItemKeyedDataSource
 */
class UsersDataSource(
    private val repository: UsersRepository,
    private val hasInternet: LiveData<Boolean>,
    private val onLoading: (Boolean) -> Unit,
    private val isEmpty: (Boolean) -> Unit,
    private val onError: ((Throwable) -> Unit?)? = null
) : ItemKeyedDataSource<Int, UserData>() {

    private var disposables = CompositeDisposable()
    private var pageNumber = 0
    private var size = 5

    var params: LoadParams<Int>? = null
    var callback: LoadCallback<UserData>? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<UserData>
    ) {
        repository.getUsers(pageNumber, hasInternet.value ?: false, size)
            .ioToMainSubscribe(
                onSubscribe = { onLoading.invoke(true) },
                onSuccess = {
                    it?.apply {
                        onUsersFetched(this, callback)
                    }
                    onLoading.invoke(false)
                },
                onError = {
                    onCallError(it)
                    onLoading.invoke(false)
                }
            ).addTo(disposables)
    }

    private fun onUsersFetched(users: List<UserData>, callback: LoadInitialCallback<UserData>) {
        pageNumber += size
        isEmpty.invoke(users.isEmpty())
        if (users.isNotEmpty()) {
            //userDao.insert(users).ioToMainSubscribe() { it.printStackTrace() }.addTo(disposables)
            repository.insertUsers(users).ioToMainSubscribe() { it.printStackTrace() }
                .addTo(disposables)
        }
        callback.onResult(users)
    }

    private fun onCallError(throwable: Throwable) {
        throwable.printStackTrace()
        onError?.invoke(throwable)
        isEmpty.invoke(true)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<UserData>) {
        this.params = params
        this.callback = callback

        repository.getUsers(params.key, hasInternet.value ?: false, size)
            .ioToMainSubscribe(
                onSubscribe = { onLoading.invoke(true) },
                onSuccess = {
                    it?.apply {
                        onMoreUsersFetched(this, callback)
                    }
                    onLoading.invoke(false)
                },
                onError = {
                    onPaginationError(it)
                    onLoading.invoke(false)
                }
            ).addTo(disposables)

    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<UserData>) {}

    override fun getKey(item: UserData): Int = pageNumber

    private fun onMoreUsersFetched(users: List<UserData>, callback: LoadCallback<UserData>) {
        pageNumber += size
        if (users.isNotEmpty()) {
            repository.insertUsers(users).ioToMainSubscribe() { it.printStackTrace() }
                .addTo(disposables)
        }
        callback.onResult(users)
    }

    private fun onPaginationError(throwable: Throwable) {
        throwable.printStackTrace()
        onError?.invoke(throwable)
    }

    fun clear() {
        pageNumber = 0
        disposables.clear()
    }
}

package com.technologies.gwallassessment.core.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.technologies.gwallassessment.core.data.db.dao.LikeDataDao
import com.technologies.gwallassessment.core.data.db.dao.UserDataDao
import com.technologies.gwallassessment.core.data.db.entity.LikeData
import com.technologies.gwallassessment.core.data.db.entity.UserData

@Database(
    entities = [UserData::class, LikeData::class],
    version = AppDatabase.VERSION, exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "goodwallassessment.db"
        const val VERSION = 1
    }

    abstract fun userDataDao(): UserDataDao
    abstract fun likeDataDao(): LikeDataDao
}
package com.technologies.gwallassessment.core.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.technologies.gwallassessment.core.data.definition.Table

@Entity(tableName = Table.LIKES)
data class LikeData(
    @PrimaryKey
    @ColumnInfo(name = "userId")
    val userId: Int
)
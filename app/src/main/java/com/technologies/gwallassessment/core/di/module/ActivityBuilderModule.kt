package com.technologies.gwallassessment.core.di.module

import com.technologies.gwallassessment.core.di.scope.ActivityScope
import com.technologies.gwallassessment.feature.dashboard.DashboardActivity
import com.technologies.gwallassessment.feature.dashboard.userdetails.UserDetailsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * For activity injection
 *
 * Add @ActivityScope and @ContributesAndroidInjector annotations for each
 */
@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindDashboardActivity(): DashboardActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindUserDetailsActivity(): UserDetailsActivity


}
package com.technologies.gwallassessment.core.di.module

import android.content.Context
import androidx.room.Room
import com.technologies.gwallassessment.core.data.db.AppDatabase
import com.technologies.gwallassessment.core.data.db.dao.LikeDataDao
import com.technologies.gwallassessment.core.data.db.dao.UserDataDao
import com.technologies.gwallassessment.core.di.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {

    @AppScope
    @Provides
    fun provideRoomDatabase(context: Context): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, AppDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideUserDataDao(appDataBase: AppDatabase): UserDataDao {
        return appDataBase.userDataDao()
    }

    @Provides
    fun provideLikeDataDao(appDataBase: AppDatabase): LikeDataDao {
        return appDataBase.likeDataDao()
    }


}
package com.technologies.gwallassessment.core.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.core.data.definition.Table
import io.reactivex.Single

@Dao
interface UserDataDao: BaseDao<UserData> {

    @Query("SELECT * FROM ${Table.USERS} LIMIT :limit OFFSET :offset")
    fun getUsers(limit: Int, offset: Int): Single<List<UserData>>

    @Query("SELECT * FROM ${Table.USERS} WHERE login = :login")
    fun getUserById(login: String): Single<UserData>
}
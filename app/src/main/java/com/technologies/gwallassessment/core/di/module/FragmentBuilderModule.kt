package com.technologies.gwallassessment.core.di.module

import com.technologies.gwallassessment.core.di.scope.FragmentScope
import com.technologies.gwallassessment.feature.dashboard.userdetails.UserDetailsFragment
import com.technologies.gwallassessment.feature.dashboard.userlist.UsersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * For fragment injection
 *
 * Add @FragmentScope and @ContributesAndroidInjector annotations for each
 */

@Module
abstract class FragmentBuilderModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindUsersFragment(): UsersFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindUserDetailsFragment(): UserDetailsFragment

}
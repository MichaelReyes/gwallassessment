package com.technologies.gwallassessment.core.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.technologies.gwallassessment.core.data.db.entity.LikeData
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.core.data.definition.Table
import io.reactivex.Single

@Dao
interface LikeDataDao: BaseDao<LikeData> {

    @Query("SELECT * FROM ${Table.LIKES} WHERE userId = :userId")
    fun getLikeByUserId(userId: Int): Single<LikeData>

}
package com.technologies.gwallassessment.core.data.definition

@Retention(AnnotationRetention.RUNTIME)
annotation class Table {
    companion object {
        const val USERS = "TBL_USERS"
        const val LIKES = "TBL_LIKES"
    }
}
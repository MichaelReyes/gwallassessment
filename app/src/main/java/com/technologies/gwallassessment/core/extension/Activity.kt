package com.technologies.gwallassessment.core.extension

import android.app.Activity
import android.content.Intent
import android.os.Bundle


fun Activity.goToActivity(mClass: Class<*>, finishCurrentActivity: Boolean, extras: Bundle? = null,
                 withResult: Boolean = false, requestCode: Int = -1) {
    val intent = Intent(this, mClass)
    extras?.apply { intent.putExtras(this) }
    if (!withResult)
        this.startActivity(intent)
    else
        this.startActivityForResult(intent, requestCode)

    if (finishCurrentActivity)
        this.finish()

}
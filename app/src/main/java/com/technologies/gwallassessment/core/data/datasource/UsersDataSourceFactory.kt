package com.technologies.gwallassessment.core.data.datasource

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.technologies.gwallassessment.core.data.db.dao.UserDataDao
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.core.network.repo.UsersRepository
import javax.inject.Inject


class UsersDataSourceFactory @Inject constructor(
    private val repo: UsersRepository,
    private val hasInternet: LiveData<Boolean>,
    private val onLoading: (Boolean) -> Unit,
    private val isEmpty: (Boolean) -> Unit,
    private val onError: ((Throwable) -> Unit?)? = null
) : DataSource.Factory<Int, UserData>() {

    val source = MutableLiveData<UsersDataSource>()

    override fun create(): DataSource<Int, UserData> {
        val source = UsersDataSource(repo, hasInternet, onLoading, isEmpty, onError)
        this.source.postValue(source)
        return source
    }
}

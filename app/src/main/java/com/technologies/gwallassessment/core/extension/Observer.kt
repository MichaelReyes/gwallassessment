package com.technologies.gwallassessment.core.extension

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * Extension function for subscribing rx single calls.
 * To shorten call instead of always doing subscribeOn(Schedulers.io()), observeOn(AndroidSchedulers.mainThread()), etc.
 * (This is the extension class i was referring on the interview)
 */
fun <T> Single<T>.ioToMainSubscribe(
    onSubscribe: (Disposable) -> Unit,
    onSuccess: (T?) -> Unit,
    onTerminate: (() -> Unit?)? = null,
    onError: (Throwable) -> Unit
): Disposable {
    return this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe { onSubscribe.invoke(it) }
        .doAfterTerminate { onTerminate?.invoke() }
        .subscribe(
            { onSuccess.invoke(it) },
            { onError.invoke(it) }
        )
}

fun Completable.ioToMainSubscribe(
    onSubscribe: ((Disposable) -> Unit?)? = null,
    onComplete: (() -> Unit?)? = null,
    onSuccess: (() -> Unit?)? = null,
    onError: (Throwable) -> Unit
): Disposable {
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe { onSubscribe?.invoke(it) }
        .doOnComplete { onComplete?.invoke() }
        .subscribe({ onSuccess?.invoke() }, { onError.invoke(it) })
}

fun <T> PublishSubject<T>.configureInterceptorWithEmpty(timeout: Long): Observable<T> =
    this.debounce(timeout, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io()).observeOn(
        AndroidSchedulers.mainThread()
    )
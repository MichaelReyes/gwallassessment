package com.technologies.gwallassessment.core.di.module

import android.content.Context
import com.technologies.gwallassessment.core.base.App
import com.technologies.gwallassessment.core.di.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    @AppScope
    fun provideContext(app: App
    ): Context {
        return app
    }

}
package com.technologies.gwallassessment.core.network.repo

import com.technologies.gwallassessment.core.data.db.dao.LikeDataDao
import com.technologies.gwallassessment.core.data.db.dao.UserDataDao
import com.technologies.gwallassessment.core.data.db.entity.LikeData
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.core.network.AppApi
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class UsersRepository @Inject constructor(
    private val service: AppApi,
    private val userDao: UserDataDao,
    private val likeDataDao: LikeDataDao
) {

    fun getUsers(
        start: Int,
        hasInternetConnection: Boolean,
        size: Int = 0
    ): Single<List<UserData>> {
        return if (hasInternetConnection) {
            service.getUsers(start)
        } else {
            userDao.getUsers(size, start)
        }
    }

    fun getUserDetails(login: String, hasInternetConnection: Boolean): Single<UserData> {
        return if (hasInternetConnection) {
            service.getUserDetails(login)
        } else {
            userDao.getUserById(login)
        }
    }

    fun insertUserData(userData: UserData): Completable {
        return userDao.insert(userData)
    }

    fun insertUsers(users: List<UserData>): Completable {
        return userDao.insert(users)
    }

    fun getLikeByUserId(userId: Int): Single<LikeData> {
        return likeDataDao.getLikeByUserId(userId)
    }

    fun setUserLike(data: LikeData, like: Boolean): Completable {
        return if (like) {
            likeDataDao.insert(data)
        } else {
            likeDataDao.delete(data)
        }
    }

}
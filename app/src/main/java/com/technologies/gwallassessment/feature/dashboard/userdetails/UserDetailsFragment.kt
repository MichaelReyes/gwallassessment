package com.technologies.gwallassessment.feature.dashboard.userdetails

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.technologies.gwallassessment.R
import com.technologies.gwallassessment.core.base.BaseFragment
import com.technologies.gwallassessment.core.base.BaseViewModel
import com.technologies.gwallassessment.core.extension.fromJson
import com.technologies.gwallassessment.databinding.FragmentUserDetailsBinding
import kotlinx.android.synthetic.main.layout_user_details.*

class UserDetailsFragment : BaseFragment<FragmentUserDetailsBinding>() {

    private val viewModel: UserDetailsViewModel by viewModels { viewModelFactory }

    override val layoutRes = R.layout.fragment_user_details

    override fun onCreated(savedInstance: Bundle?) {
        initObserver()
        initViews()
        checkArgs()
    }

    private fun initViews() {
        user_details_iv_like.setOnCheckedChangeListener { _, isChecked ->
            viewModel.onLikeChanged(isChecked)
        }
    }

    override fun getViewModel(): BaseViewModel? = viewModel

    private fun initObserver() {
        binding.lifecycleOwner = this
        binding.vm = viewModel
    }

    private fun checkArgs() {
        if (activity?.intent?.hasExtra(UserDetailsActivity.ARGS_USER) == true) {
            viewModel.setUserDetails(
                gson.fromJson(
                    activity?.intent?.extras?.getString(UserDetailsActivity.ARGS_USER, "") ?: ""
                )
            )
        } else if (arguments?.containsKey(UserDetailsActivity.ARGS_USER) == true) {
            viewModel.setUserDetails(
                gson.fromJson(
                    arguments?.getString(
                        UserDetailsActivity.ARGS_USER,
                        ""
                    ) ?: ""
                )
            )
        }
    }
}
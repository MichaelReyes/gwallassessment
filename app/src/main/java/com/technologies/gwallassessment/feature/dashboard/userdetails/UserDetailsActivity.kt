package com.technologies.gwallassessment.feature.dashboard.userdetails

import android.os.Bundle
import com.technologies.gwallassessment.R
import com.technologies.gwallassessment.core.base.BaseActivity
import com.technologies.gwallassessment.databinding.ActivityUserDetailsBinding
import kotlinx.android.synthetic.main.activity_user_details.*

class UserDetailsActivity : BaseActivity<ActivityUserDetailsBinding>() {
    override val layoutRes = R.layout.activity_user_details

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)
        setToolbar(show = true, showBackButton = true, title = getString(R.string.title_user_details))
    }

    companion object {
        const val ACTIVITY_CODE = 1234
        const val ARGS_USER = "_args_user"
    }
}
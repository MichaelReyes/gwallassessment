package com.technologies.gwallassessment.feature.dashboard.userlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.technologies.gwallassessment.core.base.BaseViewModel
import com.technologies.gwallassessment.core.data.datasource.UsersDataSourceFactory
import com.technologies.gwallassessment.core.data.db.dao.UserDataDao
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.core.network.repo.UsersRepository
import io.reactivex.Completable
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class UsersViewModel @Inject constructor(
    private val repo: UsersRepository
) : BaseViewModel() {

    lateinit var sourceFactory: UsersDataSourceFactory

    private var _empty = MutableLiveData(true)
    var empty: LiveData<Boolean> = _empty

    var users: LiveData<PagedList<UserData>> = MutableLiveData()

    //For observing network changes and refreshing list
    private val connectionObserver = Observer<Boolean> {
        Completable.complete()
            .delay(1, TimeUnit.SECONDS)
            .doOnComplete {
                if(_empty.value == true) {
                    refreshUsers()
                }
            }
            .subscribe()
            .addTo(disposable)
    }

    init {
        hasInternetConnection.observeForever(connectionObserver)
    }

    //Initialize UserDataSourceFactory and PagedList
    fun getUsers() {
        val config = PagedList.Config.Builder()
            .setPageSize(5)
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(5)
            .build()

        sourceFactory = UsersDataSourceFactory(repo, /*repo.userDao,*/ hasInternetConnection,
            onLoading = {
                setLoading(it, post = true)
            }, isEmpty = {
                _empty.postValue(it)
            }
        ) { e ->
            handleException(e) {
                it?.apply { setError(this) }
            }
        }

        users = LivePagedListBuilder(sourceFactory, config).build()
    }

    fun refreshUsers() {
        if (::sourceFactory.isInitialized) {
            sourceFactory.source.value?.invalidate()
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (::sourceFactory.isInitialized) {
            sourceFactory.source.value?.clear()
        }
    }
}
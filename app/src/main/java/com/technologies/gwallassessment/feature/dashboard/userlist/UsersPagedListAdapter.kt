package com.technologies.gwallassessment.feature.dashboard.userlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.technologies.gwallassessment.R
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.databinding.ItemUserBinding

class UsersPagedListAdapter constructor(
    @NonNull val diffCallback: DiffUtil.ItemCallback<UserData>
) : PagedListAdapter<UserData, RecyclerView.ViewHolder>(diffCallback) {

    internal var clickListener: (UserData) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemUserBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_user, parent, false
        )
        return Holder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as Holder
        viewHolder.bind(getItem(position)!!)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding: ItemUserBinding? = DataBindingUtil.bind(itemView)

        internal fun bind(user: UserData) {
            binding?.apply {
                this.user = user
                itemView.setOnClickListener { clickListener(user) }
                executePendingBindings()
            }
        }
    }
}


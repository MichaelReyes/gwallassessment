package com.technologies.gwallassessment.feature.dashboard.userlist

import android.os.Bundle
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.faltenreich.skeletonlayout.Skeleton
import com.faltenreich.skeletonlayout.applySkeleton
import com.technologies.gwallassessment.R
import com.technologies.gwallassessment.core.base.BaseFragment
import com.technologies.gwallassessment.core.base.BaseViewModel
import com.technologies.gwallassessment.core.data.diffutil.UsersDiffUtilItemCallback
import com.technologies.gwallassessment.core.extension.goToActivity
import com.technologies.gwallassessment.core.extension.observe
import com.technologies.gwallassessment.databinding.FragmentUsersBinding
import com.technologies.gwallassessment.feature.dashboard.userdetails.UserDetailsActivity
import kotlinx.android.synthetic.main.list_users.*

class UsersFragment : BaseFragment<FragmentUsersBinding>() {

    private val viewModel: UsersViewModel by viewModels { viewModelFactory }

    lateinit var adapter: UsersPagedListAdapter

    private var twoPane = false
    private var listSkeleton: Skeleton? = null

    override val layoutRes = R.layout.fragment_users

    override fun onCreated(savedInstance: Bundle?) {

        //For checking if it's two pane or not based on rendered view
        twoPane = baseView.findViewById<ViewGroup>(R.id.nav_host_fragment) != null
        initViews()
        initObserver()
    }

    override fun getViewModel(): BaseViewModel? = viewModel

    private fun initViews() {
        adapter = UsersPagedListAdapter(UsersDiffUtilItemCallback())
        users_rv_data.adapter = adapter

        listSkeleton = users_rv_data.applySkeleton(R.layout.item_user, 5)

        users_srl_data?.setOnRefreshListener {
            users_srl_data.isRefreshing = false
            viewModel.refreshUsers()
        }

        adapter.clickListener = {
            if (twoPane) {
                val navHostFragment =
                    childFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

                navHostFragment.navController.navigate(
                    R.id.userDetailsFragment,
                    bundleOf(Pair(UserDetailsActivity.ARGS_USER, gson.toJson(it)))
                )
            } else {
                goToActivity(
                    UserDetailsActivity::class.java,
                    false,
                    bundleOf(
                        Pair(UserDetailsActivity.ARGS_USER, gson.toJson(it))
                    )
                )
            }
        }
    }

    /**
     * Observing ViewModel's livedatas
     * Binding ViewModel
     */
    private fun initObserver() {
        viewModel.apply {
            getUsers()
            observe(users) { adapter.submitList(it) }
            observe(loading, ::handleLoading)
        }

        binding.lifecycleOwner = this
        binding.vm = viewModel
    }

    /**
     * Showing progress bar base on loading state is handled by BaseFragment.
     * Here, added another handling for showing/hiding list skeleton layout
     * (just for some list loading effect)
     */
    private fun handleLoading(b: Boolean?) {
        b?.let { loading ->
            if (loading && adapter.itemCount == 0 && listSkeleton?.isSkeleton() != true) {
                listSkeleton?.showSkeleton()
            } else {
                listSkeleton?.apply {
                    if (isSkeleton()) {
                        showOriginal()
                    }
                }
            }
        }
    }
}
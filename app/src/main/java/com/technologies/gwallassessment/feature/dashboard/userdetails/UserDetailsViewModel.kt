package com.technologies.gwallassessment.feature.dashboard.userdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.room.EmptyResultSetException
import com.technologies.gwallassessment.core.base.BaseViewModel
import com.technologies.gwallassessment.core.data.db.dao.LikeDataDao
import com.technologies.gwallassessment.core.data.db.dao.UserDataDao
import com.technologies.gwallassessment.core.data.db.entity.LikeData
import com.technologies.gwallassessment.core.data.db.entity.UserData
import com.technologies.gwallassessment.core.extension.configureInterceptorWithEmpty
import com.technologies.gwallassessment.core.extension.ioToMainSubscribe
import com.technologies.gwallassessment.core.network.repo.UsersRepository
import io.reactivex.Completable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(
    private val repo: UsersRepository
) : BaseViewModel() {

    private val _userDetails = MutableLiveData<UserData>()
    val userDetails: LiveData<UserData> = _userDetails

    private val _liked = MutableLiveData(false)
    val liked: LiveData<Boolean> = _liked

    private val connectionObserver = Observer<Boolean> {
        Completable.complete()
            .delay(1, TimeUnit.SECONDS)
            .ioToMainSubscribe(
                onComplete = {
                    _userDetails.value?.let {
                        getUserDetails(it.login)
                    }
                }
            ) { e ->
                handleException(e) {
                    it?.apply { setError(this) }
                }
            }.addTo(disposable)
    }

    /** PublishSubject to handle emitting of like check change value */
    private val autoCompletePublishSubjectLike = PublishSubject.create<Boolean>()

    init {
        hasInternetConnection.observeForever(connectionObserver)

        /** To add debounce to data will emit after debounce and avoid spam */
        autoCompletePublishSubjectLike.configureInterceptorWithEmpty(1000)
            .subscribe { result -> filteredLike(result) }.addTo(disposable)
    }

    fun onLikeChanged(checked: Boolean) {
        autoCompletePublishSubjectLike.onNext(checked)
    }

    /** Will save/delete like data on db */
    fun filteredLike(checked: Boolean) {
        userDetails.value?.let { user ->
            val data = LikeData(user.id)
            repo.setUserLike(data, checked).ioToMainSubscribe(
                onSubscribe = { setLoading(true) },
                onComplete = { setLoading(false) }
            ) {
                it.printStackTrace()
            }

            _liked.value = checked
        }
    }

    fun setUserDetails(details: UserData?) {
        details?.apply {
            _userDetails.value = details
        }
    }

    /**
     * Load user details from db if no internet connection and from api if has internet connection.
     * Single.flatMap for inserting data to db after the call and fetch like data on db
     */
    fun getUserDetails(login: String) {

        repo.getUserDetails(login, hasInternetConnection.value ?: false)
            .flatMap { userData ->
                _userDetails.postValue(userData)
                if (hasInternetConnection.value == true) {
                    repo.insertUserData(userData)
                        .observeOn(Schedulers.io())
                        .andThen(repo.getLikeByUserId(userData.id))
                } else {
                    repo.getLikeByUserId(userData.id)
                }
            }.ioToMainSubscribe(
                onSubscribe = { setLoading(true) },
                onSuccess = { _liked.value = it != null },
                onTerminate = { setLoading(false) }
            ) { e ->
                e.printStackTrace()
                if (e !is EmptyResultSetException) {
                    handleException(e) {
                        it?.apply { setError(this) }
                    }
                }
            }.addTo(disposable)
    }

}
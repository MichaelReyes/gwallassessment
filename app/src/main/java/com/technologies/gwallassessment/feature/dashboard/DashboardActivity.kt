package com.technologies.gwallassessment.feature.dashboard

import android.os.Bundle
import com.technologies.gwallassessment.R
import com.technologies.gwallassessment.core.base.BaseActivity
import com.technologies.gwallassessment.databinding.ActivityDashboardBinding
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity<ActivityDashboardBinding>() {
    override val layoutRes: Int
        get() = R.layout.activity_dashboard

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)
        setToolbar(show = true, showBackButton = false, title = getString(R.string.title_users))
    }

}